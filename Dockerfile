FROM python:3.11-bullseye

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . .

EXPOSE 8089

ENTRYPOINT [ "locust", "-f", "locustfile.py" ]
CMD [ "--host=https://my.example.com" ]
