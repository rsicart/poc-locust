# Poc locust

Replace example.com by the target website

Build and launch locust

```
docker build -t poc-locust .
docker run -ti --rm -p8089:8089 poc-locust
```

Open http://localhost:8089
