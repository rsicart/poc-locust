import time
from locust import HttpUser, task, between, constant_pacing

class QuickstartUser(HttpUser):
    #wait_time = between(1, 3)
    wait_time = constant_pacing(5)

    @task(1)
    def index_page(self):
        #cachebuster = int(time.time())
        #self.client.get("/?cachebuster={}".format(cachebuster))
        self.client.get("/")
